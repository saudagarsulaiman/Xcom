package com.ecoinomi.xcom.ServiceAPIs;

/*
 * Created by Sulaiman on 26/10/2018.
 */

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IcoTokenControllerApi {



//    @FormUrlEncoded
//    @POST("/api/ico/balance_of_address")
//    Call<ResponseBody> getBookDetailsResponse(@Field("maximumRows") int xMaxRows, @Field("startRowIndex") int xPageNum);

    @GET("/api/ico/get_account_wallet")
    Call<ResponseBody> getAccountWallet(@Header("Authorization") String AuthorizationDX);

    @GET("/api/ico/get_all_transactions")
    Call<ResponseBody> getTransactions(@Header("Authorization") String AuthorizationDX);

    @Headers("Content-Type: application/json")
    @POST("/api/ico/transfer")
    Call<ResponseBody> transferCoins(@Body String body, @Header("Authorization") String AuthorizationDX);

}
