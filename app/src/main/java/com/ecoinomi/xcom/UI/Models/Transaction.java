package com.ecoinomi.xcom.UI.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Transaction implements Parcelable {

    @SerializedName("id")
    int int_data_id;
    @SerializedName("txnHash")
    String str_data_txnHash;
    @SerializedName("toAddress")
    String str_data_toAddress;
    @SerializedName("txnDate")
    String str_data_txnDate;
    @SerializedName("coinValue")
    double dbl_data_coinValue;
    @SerializedName("account")
    String str_data_account;
    @SerializedName("icoTokenwallet")
    String str_data_icoTokenwallet;


    public Transaction(int int_data_id, String str_data_txnHash, String str_data_toAddress, String str_data_txnDate, double dbl_data_coinValue, String str_data_account, String str_data_icoTokenwallet) {
        this.int_data_id = int_data_id;
        this.str_data_txnHash = str_data_txnHash;
        this.str_data_toAddress = str_data_toAddress;
        this.str_data_txnDate = str_data_txnDate;
        this.dbl_data_coinValue = dbl_data_coinValue;
        this.str_data_account = str_data_account;
        this.str_data_icoTokenwallet = str_data_icoTokenwallet;
    }

    protected Transaction(Parcel in) {
        int_data_id = in.readInt();
        str_data_txnHash = in.readString();
        str_data_toAddress = in.readString();
        str_data_txnDate = in.readString();
        dbl_data_coinValue = in.readDouble();
        str_data_account = in.readString();
        str_data_icoTokenwallet = in.readString();
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    public int getInt_data_id() {
        return int_data_id;
    }

    public void setInt_data_id(int int_data_id) {
        this.int_data_id = int_data_id;
    }

    public String getStr_data_txnHash() {
        return str_data_txnHash;
    }

    public void setStr_data_txnHash(String str_data_txnHash) {
        this.str_data_txnHash = str_data_txnHash;
    }

    public String getStr_data_toAddress() {
        return str_data_toAddress;
    }

    public void setStr_data_toAddress(String str_data_toAddress) {
        this.str_data_toAddress = str_data_toAddress;
    }

    public String getStr_data_txnDate() {
        return str_data_txnDate;
    }

    public void setStr_data_txnDate(String str_data_txnDate) {
        this.str_data_txnDate = str_data_txnDate;
    }

    public double getDbl_data_coinValue() {
        return dbl_data_coinValue;
    }

    public void setDbl_data_coinValue(double dbl_data_coinValue) {
        this.dbl_data_coinValue = dbl_data_coinValue;
    }

    public String getStr_data_account() {
        return str_data_account;
    }

    public void setStr_data_account(String str_data_account) {
        this.str_data_account = str_data_account;
    }

    public String getStr_data_icoTokenwallet() {
        return str_data_icoTokenwallet;
    }

    public void setStr_data_icoTokenwallet(String str_data_icoTokenwallet) {
        this.str_data_icoTokenwallet = str_data_icoTokenwallet;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(int_data_id);
        dest.writeString(str_data_txnHash);
        dest.writeString(str_data_toAddress);
        dest.writeString(str_data_txnDate);
        dest.writeDouble(dbl_data_coinValue);
        dest.writeString(str_data_account);
        dest.writeString(str_data_icoTokenwallet);
    }
}
