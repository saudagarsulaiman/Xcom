package com.ecoinomi.xcom.UI.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ecoinomi.xcom.R;
import com.ecoinomi.xcom.ServiceAPIs.IcoTokenControllerApi;
import com.ecoinomi.xcom.UI.Models.AccountWallet;
import com.ecoinomi.xcom.UI.Models.Erc20Token;
import com.ecoinomi.xcom.Utilities.CONSTANTS;
import com.ecoinomi.xcom.Utilities.CommonUtilities;
import com.ecoinomi.xcom.Utilities.XcomApiClient;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFragment extends Fragment {

    View view;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ProgressDialog progressDialog;

    @BindView(R.id.img_logo)
    ImageView img_logo;
    @BindView(R.id.txt_total_bal)
    TextView txt_total_bal;
    @BindView(R.id.txt_eth_bal)
    TextView txt_eth_bal;

    ArrayList<AccountWallet> accountWallet;

    String loginResponseMsg, loginResponseStatus, loginResponseData;
    String str_data_address, str_data_privatekey, str_data_passcode, str_erc20_tokenName, str_erc20_tokenSymbol,
            str_erc20_tokenAddress, str_erc20_logo, str_erc_token;
    double dbl_data_balance, dbl_data_ethbalance, dbl_erc20_decimals, dbl_erc20_usdValue;
    int int_data_id, int_erc20_id;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.dashboard_fragment, container, false);
        ButterKnife.bind(this, view);
        sharedPreferences = getActivity().getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        accountWallet = new ArrayList<>();
        if (CommonUtilities.isConnectionAvailable(getActivity())) {
            invokeWallet();
        } else {
            CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.internetconnection));
        }


        return view;
    }

    private void invokeWallet() {
        try {
            String token = sharedPreferences.getString(CONSTANTS.token, null);
            progressDialog = ProgressDialog.show(getActivity(), "", getResources().getString(R.string.please_wait), true);
            IcoTokenControllerApi apiService = XcomApiClient.getClient().create(IcoTokenControllerApi.class);
            Call<ResponseBody> apiResponse = apiService.getAccountWallet(CONSTANTS.DeviantMulti + token);
            apiResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        String responsevalue = response.body().string();
                        if (!responsevalue.isEmpty() && responsevalue != null) {

                            JSONObject jsonObject = new JSONObject(responsevalue);
                            loginResponseMsg = jsonObject.getString("msg");
                            loginResponseStatus = jsonObject.getString("status");

                            if (loginResponseStatus.equals("true")) {
                                loginResponseData = jsonObject.getString("data");
                                progressDialog.dismiss();
//                                AccountWallet[] accWal = GsonUtils.getInstance().fromJson(loginResponseData, AccountWallet[].class);
//                                accountWallet = new ArrayList<AccountWallet>(Arrays.asList(accWal));
//                                Picasso.with(getActivity()).load(accountWallet.get(0).getErc20Token().getStr_erc20_logo()).into(img_logo);
//                                txt_total_bal.setText(String.format("%.4f", accountWallet.get(0).getDbl_data_balance()) + accountWallet.get(0).getErc20Token().getStr_erc20_tokenSymbol());
//                                txt_eth_bal.setText(String.format("%.4f", accountWallet.get(0).getDbl_data_ethbalance()) + "ETH");
//                                editor.putString(CONSTANTS.address, accountWallet.get(0).getStr_data_address());
//                                editor.apply();
                                JSONObject jsonObject1 = new JSONObject(loginResponseData);

                                try {
                                    int_data_id = jsonObject1.getInt("id");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    str_data_address = jsonObject1.getString("address");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    str_data_privatekey = jsonObject1.getString("privatekey");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    str_data_passcode = jsonObject1.getString("passcode");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    dbl_data_balance = jsonObject1.getDouble("balance");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    dbl_data_ethbalance = jsonObject1.getDouble("ethbalance");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    str_erc_token = jsonObject1.getString("erc20Token");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                JSONObject jsonObjectErc20 = new JSONObject(str_erc_token);

                                try {
                                    int_erc20_id = jsonObjectErc20.getInt("id");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    str_erc20_tokenName = jsonObjectErc20.getString("tokenName");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    str_erc20_tokenSymbol = jsonObjectErc20.getString("tokenSymbol");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    str_erc20_tokenAddress = jsonObjectErc20.getString("tokenAddress");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    dbl_erc20_decimals = jsonObjectErc20.getDouble("decimals");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    str_erc20_logo = jsonObjectErc20.getString("logo");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    dbl_erc20_usdValue = jsonObjectErc20.getDouble("usdValue");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Erc20Token erc20Token = new Erc20Token(int_erc20_id, str_erc20_tokenName, str_erc20_tokenSymbol,
                                        str_erc20_tokenAddress, dbl_erc20_decimals, str_erc20_logo, dbl_erc20_usdValue);
                                accountWallet.add(new AccountWallet(int_data_id, str_data_address, str_data_privatekey,
                                        str_data_passcode, dbl_data_balance, dbl_data_ethbalance, erc20Token));

                                Picasso.with(getActivity()).load(accountWallet.get(0).getErc20Token().getStr_erc20_logo()).into(img_logo);
                                txt_total_bal.setText(String.format("%.2f", accountWallet.get(0).getDbl_data_balance()));
                                txt_eth_bal.setText(String.format("%.4f", accountWallet.get(0).getDbl_data_ethbalance()));

                                editor.putString(CONSTANTS.address, accountWallet.get(0).getStr_data_address());
                                editor.putString(CONSTANTS.balance, accountWallet.get(0).getDbl_data_balance() + "");
                                editor.putString(CONSTANTS.ethBal, accountWallet.get(0).getDbl_data_ethbalance() + "");
                                editor.putString(CONSTANTS.coinName, accountWallet.get(0).getErc20Token().getStr_erc20_tokenName());
                                editor.putString(CONSTANTS.coinCode, accountWallet.get(0).getErc20Token().getStr_erc20_tokenSymbol());
                                editor.putString(CONSTANTS.coinTokenAdd, accountWallet.get(0).getErc20Token().getStr_erc20_tokenAddress());
                                editor.putString(CONSTANTS.coinDecimals, accountWallet.get(0).getErc20Token().getDbl_erc20_decimals() + "");
                                editor.putString(CONSTANTS.coinLogo, accountWallet.get(0).getErc20Token().getStr_erc20_logo());
                                editor.putString(CONSTANTS.usd_val, accountWallet.get(0).getErc20Token().getDbl_erc20_usdValue() + "");
                                editor.apply();

                            } else {
                                progressDialog.dismiss();
                                CommonUtilities.ShowToastMessage(getActivity(), loginResponseMsg);
                            }

                        } else {
                            progressDialog.dismiss();
                            CommonUtilities.ShowToastMessage(getActivity(), loginResponseMsg);
//                            Toast.makeText(getApplicationContext(), responsevalue, Toast.LENGTH_LONG).show();
                            Log.i(CONSTANTS.TAG, "onResponse:\n" + responsevalue);
                        }

                    } catch (Exception e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    if (t instanceof SocketTimeoutException) {
                        CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.Timeout));
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.Timeout), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    } else if (t instanceof java.net.ConnectException) {
                        CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.networkerror));
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), getResources().getString(R.string.networkerror), Toast.LENGTH_SHORT).show();
                    } else {
                        CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.errortxt));
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.errortxt), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }
            });
        } catch (Exception ex) {
            progressDialog.dismiss();
            ex.printStackTrace();
            CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.errortxt));
//            Toast.makeText(getApplicationContext(), getResources().getString(R.string.errortxt), Toast.LENGTH_SHORT).show();
        }

    }

}
