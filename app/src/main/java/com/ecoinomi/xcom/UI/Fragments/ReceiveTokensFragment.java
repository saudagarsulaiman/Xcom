package com.ecoinomi.xcom.UI.Fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecoinomi.xcom.R;
import com.ecoinomi.xcom.Utilities.CONSTANTS;
import com.ecoinomi.xcom.Utilities.CommonUtilities;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReceiveTokensFragment extends Fragment {

    View view;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @BindView(R.id.img_coin_logo)
    ImageView img_coin_logo;
    @BindView(R.id.txt_coin_name)
    TextView txt_coin_name;
    @BindView(R.id.img_qrcode)
    ImageView img_qrcode;
    @BindView(R.id.txt_coin_code)
    TextView txt_coin_code;
    @BindView(R.id.txt_address)
    TextView txt_address;
    @BindView(R.id.img_copy_address)
    ImageView img_copy_address;
    @BindView(R.id.btn_share_qrcode)
    Button btn_share_qrcode;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.receive_tokens_fragment, container, false);
        ButterKnife.bind(this, view);
        sharedPreferences = getActivity().getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();


        Picasso.with(getActivity()).load(sharedPreferences.getString(CONSTANTS.coinLogo, "")).into(img_coin_logo);
        txt_coin_name.setText(sharedPreferences.getString(CONSTANTS.coinName, ""));
        txt_coin_code.setText(sharedPreferences.getString(CONSTANTS.coinCode, ""));
        txt_address.setText(sharedPreferences.getString(CONSTANTS.address, ""));

//           QR Code Generator
        CommonUtilities.qrCodeGenerate(sharedPreferences.getString(CONSTANTS.address, ""), img_qrcode, getActivity());

        btn_share_qrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//           Sharing Address
                CommonUtilities.shareAddress(sharedPreferences.getString(CONSTANTS.address, ""), getActivity());
            }
        });

        img_copy_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtilities.copyToClipboard(getActivity(), sharedPreferences.getString(CONSTANTS.address, ""), sharedPreferences.getString(CONSTANTS.coinName, ""));
            }
        });


        return view;
    }

}
