package com.ecoinomi.xcom.UI.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ecoinomi.xcom.R;
import com.ecoinomi.xcom.ServiceAPIs.IcoTokenControllerApi;
import com.ecoinomi.xcom.UI.Activities.DashBoardActivity;
import com.ecoinomi.xcom.Utilities.CONSTANTS;
import com.ecoinomi.xcom.Utilities.CommonUtilities;
import com.ecoinomi.xcom.Utilities.XcomApiClient;
import com.google.zxing.Result;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendTokensFragment extends Fragment implements ZXingScannerView.ResultHandler {

    View view;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ProgressDialog progressDialog;

    @BindView(R.id.img_coin_logo)
    ImageView img_coin_logo;
    @BindView(R.id.txt_coin_name)
    TextView txt_coin_name;
    @BindView(R.id.txt_coin_code)
    TextView txt_coin_code;
    @BindView(R.id.btn_send)
    Button btn_send;
    @BindView(R.id.edt_amount_bal)
    EditText edt_amount_bal;
    @BindView(R.id.img_scanner)
    ImageView img_scanner;
    @BindView(R.id.edt_btcp_address)
    EditText edt_btcp_address;
    @BindView(R.id.txt_amount_code)
    TextView txt_amount_code;
    @BindView(R.id.scan_qr)
    ZXingScannerView mScannerView;
    @BindView(R.id.scan_view)
    RelativeLayout mScannerLayout;
    @BindView(R.id.txt_total_bal)
    TextView txt_total_bal;
    @BindView(R.id.txt_eth_bal)
    TextView txt_eth_bal;

    Double usdCoinValue = 0.0;


    String loginResponseMsg, loginResponseStatus, loginResponseData;

    Boolean isEditFiat = false, isEditAmount = false;

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

   /* public static int PERMISSION_ALL = 1;
    public static String[] PERMISSIONS = {Manifest.permission.CAMERA};

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
*/
    String balance,ethBalance;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.send_tokens_fragment, container, false);
        ButterKnife.bind(this, view);
        sharedPreferences = getActivity().getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        balance = sharedPreferences.getString(CONSTANTS.balance, "");
        ethBalance=sharedPreferences.getString(CONSTANTS.ethBal, "");
        double bal = Double.parseDouble(balance);
        final double ethBal = Double.parseDouble(ethBalance);
        Picasso.with(getActivity()).load(sharedPreferences.getString(CONSTANTS.coinLogo, "")).into(img_coin_logo);
        txt_coin_name.setText(sharedPreferences.getString(CONSTANTS.coinName, ""));
        txt_coin_code.setText(sharedPreferences.getString(CONSTANTS.coinCode, ""));
        txt_total_bal.setText(String.format("%.2f", bal));
        txt_eth_bal.setText(String.format("%.4f", ethBal));

        edt_amount_bal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                convertAmountToCoin(s.toString().trim());
            }
        });
        edt_amount_bal.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                isEditAmount = b;
            }
        });

        img_scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                startActivityForResult(intent, 0);
//                if (hasPermissions(getActivity(), PERMISSIONS)) {
//                    mScannerLayout.setVisibility(View.VISIBLE);
//                    mScannerView.setResultHandler(getActivity()); // Register ourselves as a handler for scan results.<br />
//                    mScannerView.startCamera();
//                } else {
//                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
//                }
            }
        });

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edt_amount_bal.getText().toString().isEmpty()) {
                    try {
                        if (Double.parseDouble(edt_amount_bal.getText().toString().trim()) > 0) {
                            String send_bal = edt_amount_bal.getText().toString();
//                            String fiat_bal = edt_fiat_bal.getText().toString();
//                                String fee = "0.0001";
                            Double ttl_rcv = Double.parseDouble(send_bal)/* - Double.parseDouble(fee)*/;

                            String str_btcp_address = edt_btcp_address.getText().toString();

                            if (!str_btcp_address.isEmpty() && !send_bal.isEmpty()) {
//                                customDialog( send_bal, /*fiat_bal, *//*fee, */ttl_rcv, str_btcp_address);
                                double bal = Double.parseDouble(sharedPreferences.getString(CONSTANTS.balance, ""));
                                if (ttl_rcv < bal) {
                                    if (ethBal > 0.0001) {
                                        SendingCoins(sharedPreferences.getString(CONSTANTS.address, ""), str_btcp_address, ttl_rcv);
                                    } else {
                                        CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.insufficient_eth));
                                    }
                                } else {
                                    CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.insufficient_fund));
                                }
                            } else {
                                CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.enter_every_detail));
                            }
                        } else {
                            CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.enter_amount));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.enter_amount));
                }
            }
        });


        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == getActivity().RESULT_OK) {

                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                Toast.makeText(getActivity(), "SCAN_RESULT --- >>>   " + contents,
                        Toast.LENGTH_LONG).show();
                // Handle successful scan
                edt_btcp_address.setText(contents);

            } else if (resultCode == getActivity().RESULT_CANCELED) {
                // Handle cancel
                Log.i("App", "Scan unsuccessful");
            }
        }
    }

//    private void convertAmountToCoin(String amountTextValue) {
//        if (isEditAmount) {
//            if (!amountTextValue.trim().isEmpty()) {
//                try {
//                    if (Double.parseDouble(amountTextValue) != 0) {
//                        Double finalValue = Double.parseDouble(amountTextValue);
//                        if (selectedAccountWallet.getStr_data_balance() > finalValue) {
//                            edt_fiat_bal.setText(String.format("%.4f", (usdCoinValue * finalValue)));
//                        } else {
//                            CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.insufficient_fund));
//                            edt_fiat_bal.setText("0");
//                            edt_amount_bal.setText("0");
//                        }
//                    } else {
//                        edt_fiat_bal.setText("0");
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    edt_fiat_bal.setText("0");
//                }
//            } else {
//                edt_fiat_bal.setText("0");
//            }
//        }
//    }


//    private void customDialog(String send_bal, String fiat_bal/*, String fee*/, final Double ttl_rcv, final String toAddress) {
//        //                Creating A Custom Dialog Using DialogPlus
//        ViewHolder viewHolder = new ViewHolder(R.layout.dialog_send_confirm);
//        final DialogPlus dialog = DialogPlus.newDialog(getActivity())
//                .setContentHolder(viewHolder)
//                .setGravity(Gravity.BOTTOM)
//                .setCancelable(true)
//                .setInAnimation(R.anim.slide_in_bottom)
//                .setOutAnimation(R.anim.slide_out_bottom)
//                .setContentWidth(ViewGroup.LayoutParams.MATCH_PARENT)
//                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
////                        .setOnDismissListener(new OnDismissListener() {
////                            @Override
////                            public void onDismiss(DialogPlus dialog) {
////
////                            }
////                        })
////                        .setExpanded(true) // default is false, only works for grid and list
//                .create();
//
////                Initializing Widgets
//        View view = dialog.getHolderView();
//        TextView txt_cancel = view.findViewById(R.id.txt_cancel);
//        TextView txt_send = view.findViewById(R.id.txt_send);
//
//        TextView txt_amount_bal = view.findViewById(R.id.txt_amount_bal);
//        TextView txt_amount_code = view.findViewById(R.id.txt_amount_code);
//        TextView txt_fiat_bal = view.findViewById(R.id.txt_fiat_bal);
//        TextView txt_fiat_code = view.findViewById(R.id.txt_fiat_code);
//        TextView txt_to_address = view.findViewById(R.id.txt_to_address);
//        TextView txt_fee = view.findViewById(R.id.txt_fee);
//        TextView txt_fee_code = view.findViewById(R.id.txt_fee_code);
//        TextView txt_ttl_receive = view.findViewById(R.id.txt_ttl_receive);
//        TextView txt_ttl_receive_code = view.findViewById(R.id.txt_ttl_receive_code);
//
//        txt_amount_bal.setText(send_bal);
//        txt_amount_code.setText(selectedAccountWallet.getAllCoins().getStr_coin_code());
//        txt_fiat_bal.setText(fiat_bal);
////        txt_fiat_code.setText();
//        txt_to_address.setText(toAddress);
////        txt_fee.setText(fee);
//        txt_fee_code.setText(selectedAccountWallet.getAllCoins().getStr_coin_code());
//        txt_ttl_receive.setText("" + ttl_rcv);
//        txt_ttl_receive_code.setText(selectedAccountWallet.getAllCoins().getStr_coin_code());
//
//        txt_cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        txt_send.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////            Senidng Coins
//                SendingCoins(selectedAccountWallet.getStr_data_address(), toAddress, ttl_rcv);
//                dialog.dismiss();
//
//            }
//        });
////                Displaying DialogPlus
//        dialog.show();
//
//    }


    private void SendingCoins(String fromAddress, String toAddress, Double amount) {
        try {
            JSONObject params = new JSONObject();
            try {
                params.put("fromAddress", fromAddress);
//                Log.e("fromAddress:", fromAddress);
                params.put("toAddress", toAddress);
                params.put("amount", amount);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String token = sharedPreferences.getString(CONSTANTS.token, null);
            progressDialog = ProgressDialog.show(getActivity(), "", getResources().getString(R.string.please_wait), true);
            IcoTokenControllerApi apiService = XcomApiClient.getClient().create(IcoTokenControllerApi.class);
            Call<ResponseBody> apiResponse = apiService.transferCoins(params.toString(), CONSTANTS.DeviantMulti + token);
            apiResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        String responsevalue = response.body().string();

                        if (!responsevalue.isEmpty() && responsevalue != null) {
                            progressDialog.dismiss();

                            JSONObject jsonObject = new JSONObject(responsevalue);
                            loginResponseMsg = jsonObject.getString("msg");
                            loginResponseStatus = jsonObject.getString("status");

                            if (loginResponseStatus.equals("true")) {
                                loginResponseData = jsonObject.getString("data");

                                CommonUtilities.ShowToastMessage(getActivity(), loginResponseMsg);

                                Intent intent = new Intent(getActivity(), DashBoardActivity.class);
                                intent.putExtra(CONSTANTS.seletedTab, 2);
                                startActivity(intent);

                            } else {
//                                Intent intent = new Intent(getActivity(), DashBoardActivity.class);
//                                startActivity(intent);
                                progressDialog.dismiss();
                                CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.transaction) + loginResponseMsg);
                            }

                        } else {
                            progressDialog.dismiss();
                            CommonUtilities.ShowToastMessage(getActivity(), loginResponseMsg);
//                            Toast.makeText(getActivity(), responsevalue, Toast.LENGTH_LONG).show();
                            Log.i(CONSTANTS.TAG, "onResponse:\n" + responsevalue);
                        }

                    } catch (Exception e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
//                        progressDialog.dismiss();
                        CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.errortxt));
//                        Toast.makeText(getActivity(), getResources().getString(R.string.errortxt), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    if (t instanceof SocketTimeoutException) {
                        progressDialog.dismiss();
                        CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.Timeout));
//                        Toast.makeText(getActivity(), getResources().getString(R.string.Timeout), Toast.LENGTH_SHORT).show();
                    } else if (t instanceof java.net.ConnectException) {
                        progressDialog.dismiss();
                        CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.networkerror));
                        Toast.makeText(getActivity(), getResources().getString(R.string.networkerror), Toast.LENGTH_SHORT).show();
                    } else {
                        progressDialog.dismiss();
                        CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.errortxt));
//                        Toast.makeText(getActivity(), getResources().getString(R.string.errortxt), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception ex) {
            progressDialog.dismiss();
            ex.printStackTrace();
            CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.errortxt));
//            Toast.makeText(getActivity(), getResources().getString(R.string.errortxt), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void handleResult(Result result) {
        edt_btcp_address.setText(result.getText());
        mScannerLayout.setVisibility(View.GONE);
    }


}
