package com.ecoinomi.xcom.UI.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class AccountWallet implements Parcelable {

    @SerializedName("id")
    int int_data_id;
    @SerializedName("address")
    String str_data_address;
    @SerializedName("privatekey")
    String str_data_privatekey;
    @SerializedName("passcode")
    String str_data_passcode;
    @SerializedName("balance")
    double dbl_data_balance;
    @SerializedName("ethbalance")
    double dbl_data_ethbalance;
    @SerializedName("erc20Token")
    Erc20Token erc20Token;

    public AccountWallet(int int_data_id, String str_data_address, String str_data_privatekey, String str_data_passcode, double dbl_data_balance, double dbl_data_ethbalance, Erc20Token erc20Token) {
        this.int_data_id = int_data_id;
        this.str_data_address = str_data_address;
        this.str_data_privatekey = str_data_privatekey;
        this.str_data_passcode = str_data_passcode;
        this.dbl_data_balance = dbl_data_balance;
        this.dbl_data_ethbalance = dbl_data_ethbalance;
        this.erc20Token = erc20Token;
    }

    protected AccountWallet(Parcel in) {
        int_data_id = in.readInt();
        str_data_address = in.readString();
        str_data_privatekey = in.readString();
        str_data_passcode = in.readString();
        dbl_data_balance = in.readDouble();
        dbl_data_ethbalance = in.readDouble();
        erc20Token = in.readParcelable(Erc20Token.class.getClassLoader());
    }

    public static final Creator<AccountWallet> CREATOR = new Creator<AccountWallet>() {
        @Override
        public AccountWallet createFromParcel(Parcel in) {
            return new AccountWallet(in);
        }

        @Override
        public AccountWallet[] newArray(int size) {
            return new AccountWallet[size];
        }
    };

    public int getInt_data_id() {
        return int_data_id;
    }

    public void setInt_data_id(int int_data_id) {
        this.int_data_id = int_data_id;
    }

    public String getStr_data_address() {
        return str_data_address;
    }

    public void setStr_data_address(String str_data_address) {
        this.str_data_address = str_data_address;
    }

    public String getStr_data_privatekey() {
        return str_data_privatekey;
    }

    public void setStr_data_privatekey(String str_data_privatekey) {
        this.str_data_privatekey = str_data_privatekey;
    }

    public String getStr_data_passcode() {
        return str_data_passcode;
    }

    public void setStr_data_passcode(String str_data_passcode) {
        this.str_data_passcode = str_data_passcode;
    }

    public double getDbl_data_balance() {
        return dbl_data_balance;
    }

    public void setDbl_data_balance(double dbl_data_balance) {
        this.dbl_data_balance = dbl_data_balance;
    }

    public double getDbl_data_ethbalance() {
        return dbl_data_ethbalance;
    }

    public void setDbl_data_ethbalance(double dbl_data_ethbalance) {
        this.dbl_data_ethbalance = dbl_data_ethbalance;
    }

    public Erc20Token getErc20Token() {
        return erc20Token;
    }

    public void setErc20Token(Erc20Token erc20Token) {
        this.erc20Token = erc20Token;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(int_data_id);
        dest.writeString(str_data_address);
        dest.writeString(str_data_privatekey);
        dest.writeString(str_data_passcode);
        dest.writeDouble(dbl_data_balance);
        dest.writeDouble(dbl_data_ethbalance);
        dest.writeParcelable(erc20Token, flags);
    }
}
