package com.ecoinomi.xcom.UI.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ecoinomi.xcom.R;
import com.ecoinomi.xcom.UI.Fragments.DashboardFragment;
import com.ecoinomi.xcom.UI.Fragments.ReceiveTokensFragment;
import com.ecoinomi.xcom.UI.Fragments.SendTokensFragment;
import com.ecoinomi.xcom.UI.Fragments.SettingsFragment;
import com.ecoinomi.xcom.UI.Fragments.TransactionHistoryFragment;
import com.ecoinomi.xcom.Utilities.CONSTANTS;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.TriangularPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ecoinomi.xcom.Utilities.MyApplication.myApplication;

public class DashBoardActivity extends AppCompatActivity {


    private Boolean exit = false;
    @Nullable
    @BindView(R.id.magic_indicator)
    MagicIndicator magicIndicator;
    @Nullable
    @BindView(R.id.txt_btm_nav_lbl)
    TextView txt_btm_nav_lbl;
    @Nullable
    @BindView(R.id.tool_nav)
    Toolbar toolbar_nav;
    @Nullable
    @BindView(R.id.txt_tlbr_title)
    TextView txt_tlbr_title;

    int[] CHANNELSImage = new int[]{R.drawable.selector_btm_nav_send_tokens, R.drawable.selector_btm_nav_receive_tokens, R.drawable.selector_btm_nav_dashboard, R.drawable.selector_btm_nav_trans_his, R.drawable.selector_btm_nav_settings};
    int[] channelsName = new int[]{R.string.send, R.string.receive, R.string.wallet, R.string.history, R.string.settings};
    int[] channelTtlName = new int[]{R.string.send_tokens, R.string.rec_tokens, R.string.xcom_wallet, R.string.trans_history, R.string.app_settings};


    FragmentManager supportFragmentManager;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

//    @Override
//    protected void onResume() {
//        super.onResume();
//        myApplication.disableScreenCapture(this);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        ButterKnife.bind(this);

        txt_btm_nav_lbl.setText(channelsName[2]);
        txt_tlbr_title.setText(channelTtlName[2]);
        supportFragmentManager = getSupportFragmentManager();
        sharedPreferences = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        initMagicIndicator();
        int selectedTab = (getIntent().getIntExtra(CONSTANTS.seletedTab, 2));
        setAllSelection(selectedTab);

        // setupViewPager(mViewPager);
//        txt_btm_nav_lbl.setTextColor(getResources().getColor(R.color.yellow));
        txt_btm_nav_lbl.setTextColor(getResources().getColor(R.color.white));

    }

    private void initMagicIndicator() {
        final CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return CHANNELSImage == null ? 0 : CHANNELSImage.length;
            }

            /*  @Override
              public IPagerTitleView getTitleView(Context context, final int index) {
                  return new DummyPagerTitleView(context);
              }*/
            @Override
            public IPagerTitleView getTitleView(final Context context, final int index) {
                CommonPagerTitleView commonPagerTitleView = new CommonPagerTitleView(context);

                // load custom layout
                View customLayout = LayoutInflater.from(context).inflate(R.layout.simple_pager_title_layout, null);
                final ImageView titleImg = (ImageView) customLayout.findViewById(R.id.title_img);
                titleImg.setImageResource(CHANNELSImage[index]);
                commonPagerTitleView.setContentView(customLayout);

                commonPagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {

                    @Override
                    public void onSelected(int index, int totalCount) {
                        titleImg.setSelected(true);
                    }

                    @Override
                    public void onDeselected(int index, int totalCount) {
                        titleImg.setSelected(false);
                    }

                    @Override
                    public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {
//                        titleImg.setScaleX(1.3f + (0.8f - 1.3f) * leavePercent);
//                        titleImg.setScaleY(1.3f + (0.8f - 1.3f) * leavePercent);
                    }

                    @Override
                    public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {
//                        titleImg.setScaleX(0.8f + (1.3f - 0.8f) * enterPercent);
//                        titleImg.setScaleY(0.8f + (1.3f - 0.8f) * enterPercent);
                    }
                });

                commonPagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setAllSelection(index);
                    }
                });

                return commonPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                TriangularPagerIndicator indicator = new TriangularPagerIndicator(context);
                indicator.setReverse(false);
                float smallNavigatorHeight = context.getResources().getDimension(R.dimen.small_navigator_height);
                indicator.setLineHeight(UIUtil.dip2px(context, 5));
                indicator.setTriangleHeight((int) smallNavigatorHeight);
                indicator.setLineColor(Color.parseColor("#FFFFFF"));
                return indicator;
            }
        });
        magicIndicator.setNavigator(commonNavigator);
    }

    private void setAllSelection(int index) {
        setCurrentTabFragment(index);
        if (index != 5) {
            txt_btm_nav_lbl.setText(channelsName[index]);
            txt_tlbr_title.setText(channelTtlName[index]);
            magicIndicator.onPageSelected(index);
            magicIndicator.onPageScrollStateChanged(index);
            magicIndicator.onPageScrolled(index, 0, 0);
        }
    }

    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
//                replaceFragment(new SendTokensFragment());
                loadFragment(new SendTokensFragment());
                break;
            case 1:
//                replaceFragment(new ReceiveTokensFragment());
                loadFragment(new ReceiveTokensFragment());
                break;
            case 2:
//                replaceFragment(new DashboardFragment());
                loadFragment(new DashboardFragment());
                break;
            case 3:
//                replaceFragment(new TransactionHistoryFragment());
                loadFragment(new TransactionHistoryFragment());
                break;
            case 4:
//                replaceFragment(new SettingsFragment());
                loadFragment(new SettingsFragment());
                break;
        }
    }

    public void replaceFragment(Fragment fragment) {
        /*FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.frame_container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();*/

        Fragment f1 = supportFragmentManager.findFragmentByTag(fragment.getClass().getName());

        if (f1 == null) {
            f1 = fragment;
            supportFragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .add(R.id.frame_container, f1, fragment.getClass().getName())
                    .addToBackStack(fragment.getClass().getName()).commit();
        } else {
            FragmentTransaction transaction = supportFragmentManager.beginTransaction();
            SendTokensFragment sendTokensFragment = (SendTokensFragment) supportFragmentManager.findFragmentByTag(SendTokensFragment.class.getName());
            ReceiveTokensFragment receiveTokensFragment = (ReceiveTokensFragment) supportFragmentManager.findFragmentByTag(ReceiveTokensFragment.class.getName());
            DashboardFragment dashboardFragment = (DashboardFragment) supportFragmentManager.findFragmentByTag(DashboardFragment.class.getName());
            TransactionHistoryFragment transactionHistoryFragment = (TransactionHistoryFragment) supportFragmentManager.findFragmentByTag(TransactionHistoryFragment.class.getName());
            SettingsFragment settingsFragment = (SettingsFragment) supportFragmentManager.findFragmentByTag(SettingsFragment.class.getName());

            if (sendTokensFragment != null)
                if (sendTokensFragment != f1)
                    transaction.hide(sendTokensFragment);

            if (receiveTokensFragment != null)
                if (receiveTokensFragment != f1)
                    transaction.hide(receiveTokensFragment);

            if (dashboardFragment != null)
                if (dashboardFragment != f1)
                    transaction.hide(dashboardFragment);

            if (transactionHistoryFragment != null)
                if (transactionHistoryFragment != f1)
                    transaction.hide(transactionHistoryFragment);

            if (settingsFragment != null)
                if (settingsFragment != f1)
                    transaction.hide(settingsFragment);

            transaction.show(f1);
            transaction.commit();
            // supportFragmentManager.beginTransaction()
            //.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            //  .replace(R.id.frame_container, f1, fragment.getClass().getName())
            // .addToBackStack(fragment.getClass().getName()).commit();
        }
    }

    //  Fragments Replacements
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            finishAffinity(); // Close all activites
            System.exit(0);  // Releasing resources
            Toast.makeText(this, "Logged Out Successfully.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }

}
