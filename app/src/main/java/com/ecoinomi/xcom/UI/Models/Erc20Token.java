package com.ecoinomi.xcom.UI.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Erc20Token implements Parcelable {

    @SerializedName("id")
    int int_erc20_id;
    @SerializedName("tokenName")
    String str_erc20_tokenName;
    @SerializedName("tokenSymbol")
    String str_erc20_tokenSymbol;
    @SerializedName("tokenAddress")
    String str_erc20_tokenAddress;
    @SerializedName("decimals")
    double dbl_erc20_decimals;
    @SerializedName("logo")
    String str_erc20_logo;
    @SerializedName("usdValue")
    double dbl_erc20_usdValue;

    public Erc20Token(int int_erc20_id, String str_erc20_tokenName, String str_erc20_tokenSymbol, String str_erc20_tokenAddress, double dbl_erc20_decimals, String str_erc20_logo, double dbl_erc20_usdValue) {
        this.int_erc20_id = int_erc20_id;
        this.str_erc20_tokenName = str_erc20_tokenName;
        this.str_erc20_tokenSymbol = str_erc20_tokenSymbol;
        this.str_erc20_tokenAddress = str_erc20_tokenAddress;
        this.dbl_erc20_decimals = dbl_erc20_decimals;
        this.str_erc20_logo = str_erc20_logo;
        this.dbl_erc20_usdValue = dbl_erc20_usdValue;
    }

    protected Erc20Token(Parcel in) {
        int_erc20_id = in.readInt();
        str_erc20_tokenName = in.readString();
        str_erc20_tokenSymbol = in.readString();
        str_erc20_tokenAddress = in.readString();
        dbl_erc20_decimals = in.readDouble();
        str_erc20_logo = in.readString();
        dbl_erc20_usdValue = in.readDouble();
    }

    public static final Creator<Erc20Token> CREATOR = new Creator<Erc20Token>() {
        @Override
        public Erc20Token createFromParcel(Parcel in) {
            return new Erc20Token(in);
        }

        @Override
        public Erc20Token[] newArray(int size) {
            return new Erc20Token[size];
        }
    };

    public int getInt_erc20_id() {
        return int_erc20_id;
    }

    public void setInt_erc20_id(int int_erc20_id) {
        this.int_erc20_id = int_erc20_id;
    }

    public String getStr_erc20_tokenName() {
        return str_erc20_tokenName;
    }

    public void setStr_erc20_tokenName(String str_erc20_tokenName) {
        this.str_erc20_tokenName = str_erc20_tokenName;
    }

    public String getStr_erc20_tokenSymbol() {
        return str_erc20_tokenSymbol;
    }

    public void setStr_erc20_tokenSymbol(String str_erc20_tokenSymbol) {
        this.str_erc20_tokenSymbol = str_erc20_tokenSymbol;
    }

    public String getStr_erc20_tokenAddress() {
        return str_erc20_tokenAddress;
    }

    public void setStr_erc20_tokenAddress(String str_erc20_tokenAddress) {
        this.str_erc20_tokenAddress = str_erc20_tokenAddress;
    }

    public double getDbl_erc20_decimals() {
        return dbl_erc20_decimals;
    }

    public void setDbl_erc20_decimals(double dbl_erc20_decimals) {
        this.dbl_erc20_decimals = dbl_erc20_decimals;
    }

    public String getStr_erc20_logo() {
        return str_erc20_logo;
    }

    public void setStr_erc20_logo(String str_erc20_logo) {
        this.str_erc20_logo = str_erc20_logo;
    }

    public double getDbl_erc20_usdValue() {
        return dbl_erc20_usdValue;
    }

    public void setDbl_erc20_usdValue(double dbl_erc20_usdValue) {
        this.dbl_erc20_usdValue = dbl_erc20_usdValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(int_erc20_id);
        dest.writeString(str_erc20_tokenName);
        dest.writeString(str_erc20_tokenSymbol);
        dest.writeString(str_erc20_tokenAddress);
        dest.writeDouble(dbl_erc20_decimals);
        dest.writeString(str_erc20_logo);
        dest.writeDouble(dbl_erc20_usdValue);
    }
}
