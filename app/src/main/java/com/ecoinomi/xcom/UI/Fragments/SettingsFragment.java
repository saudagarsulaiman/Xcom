package com.ecoinomi.xcom.UI.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ecoinomi.xcom.R;
import com.ecoinomi.xcom.ServiceAPIs.UserControllerApi;
import com.ecoinomi.xcom.UI.Activities.WelcomeActivity;
import com.ecoinomi.xcom.Utilities.CONSTANTS;
import com.ecoinomi.xcom.Utilities.CommonUtilities;
import com.ecoinomi.xcom.Utilities.XcomApiClient;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsFragment extends Fragment {
    View view;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ProgressDialog progressDialog;

    @BindView(R.id.lnr_change_pswd)
    LinearLayout lnr_change_pswd;
    @BindView(R.id.lnr_logout)
    LinearLayout lnr_logout;

    String tkn, loginResponseMsg, loginResponseStatus, getLoginResponseMsg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.settings_fragment, container, false);
        ButterKnife.bind(this, view);
        sharedPreferences = getActivity().getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        tkn = sharedPreferences.getString(CONSTANTS.token, "");

        lnr_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString(CONSTANTS.address, null);
                editor.putString(CONSTANTS.balance, null);
                editor.putString(CONSTANTS.ethBal, null);
                editor.putString(CONSTANTS.coinName, null);
                editor.putString(CONSTANTS.coinCode, null);
                editor.putString(CONSTANTS.coinTokenAdd, null);
                editor.putString(CONSTANTS.coinDecimals, null);
                editor.putString(CONSTANTS.coinLogo, null);
                editor.putString(CONSTANTS.usd_val, null);
                editor.putString(CONSTANTS.token, null);
                editor.putString(CONSTANTS.email, null);
                editor.putString(CONSTANTS.pswd, null);
                editor.apply();

                CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.logout_success));
                Intent intent = new Intent(getActivity(), WelcomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        lnr_change_pswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PasswordDialog(tkn);
            }
        });

        return view;
    }

    private void PasswordDialog(final String tkn) {
        ViewHolder viewHolder = new ViewHolder(R.layout.dialog_change_password);
        final DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setContentHolder(viewHolder)
                .setGravity(Gravity.BOTTOM)
                .setCancelable(true)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setContentWidth(ViewGroup.LayoutParams.MATCH_PARENT)
                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .create();

//                Initializing Widgets
        View view = dialog.getHolderView();

        final EditText edt_old_pswd = view.findViewById(R.id.edt_old_pswd);
        final EditText edt_new_pswd = view.findViewById(R.id.edt_new_pswd);
        final TextView txt_lower_case = view.findViewById(R.id.txt_lower_case);
        final TextView txt_upper_case = view.findViewById(R.id.txt_upper_case);
        final TextView txt_number = view.findViewById(R.id.txt_number);
        final TextView txt_chars = view.findViewById(R.id.txt_chars);
        final EditText edt_confirm_pswd = view.findViewById(R.id.edt_confirm_pswd);
        final ImageView img_center_back = view.findViewById(R.id.img_center_back);

        Button btn_change_pswd = view.findViewById(R.id.btn_change_pswd);


        edt_new_pswd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
//                CommonUtilities.matchingPasswordText(getActivity(), text, txt_lower_case, txt_upper_case, txt_number, txt_chars);
                matchingPasswordText(text, txt_lower_case, txt_upper_case, txt_number, txt_chars);
            }
        });

        btn_change_pswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String old_pswd = edt_old_pswd.getText().toString();
                String new_pswd = edt_new_pswd.getText().toString();
                String conf_pswd = edt_confirm_pswd.getText().toString();
                CheckingInputs(tkn, old_pswd, new_pswd, conf_pswd, dialog);
//                dialog.dismiss();
            }
        });
        img_center_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

//                Displaying DialogPlus
        dialog.show();


    }


    private void matchingPasswordText(String text, TextView txt_lower_case, TextView txt_upper_case, TextView txt_number, TextView txt_chars) {
//        if (text.matches("(?=^.{8,25}$)(?=.*\\d)(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$")) {
        if (text.matches("(?=^.{8,25}$)(?=.*\\d)(?![.\\n])(?=.*[A-Z])(?=.*[a-z])(?=.*[@#$%^&+=!*]).*$")) {
            txt_lower_case.setBackground(getResources().getDrawable(R.drawable.rec_green_c2));
            txt_upper_case.setBackground(getResources().getDrawable(R.drawable.rec_green_c2));
            txt_number.setBackground(getResources().getDrawable(R.drawable.rec_green_c2));
            txt_chars.setBackground(getResources().getDrawable(R.drawable.rec_green_c2));
        } else {
//            if (text.matches("(?![.\\n])(?=.*[a-z]).*$+")) {
            if (text.matches("(?![.\\n])(?=.*[@#$%^&+=!*]).*$+")) {
                txt_lower_case.setBackground(getResources().getDrawable(R.drawable.rec_lgreen_c2));
            } else {
                txt_lower_case.setBackground(getResources().getDrawable(R.drawable.rec_gred_c2));
            }
            if (text.matches("(?![.\\n])(?=.*[A-Z]).*$+")) {
                txt_upper_case.setBackground(getResources().getDrawable(R.drawable.rec_lgreen_c2));
            } else {
                txt_upper_case.setBackground(getResources().getDrawable(R.drawable.rec_gred_c2));
            }

            if (text.matches("(?![.\\n])(?=.*\\d).*$+")) {
                txt_number.setBackground(getResources().getDrawable(R.drawable.rec_lgreen_c2));
            } else {
                txt_number.setBackground(getResources().getDrawable(R.drawable.rec_gred_c2));
            }

            if (text.length() > 7 && text.length() < 26) {
                txt_chars.setBackground(getResources().getDrawable(R.drawable.rec_lgreen_c2));
            } else {
                txt_chars.setBackground(getResources().getDrawable(R.drawable.rec_gred_c2));
            }
        }
    }

    private void CheckingInputs(String tkn, String old_pswd, String new_pswd, String conf_pswd, DialogPlus dialog) {
        if (!old_pswd.isEmpty()) {
            if (old_pswd.equals(sharedPreferences.getString(CONSTANTS.pswd, ""))) {
                if (!new_pswd.isEmpty()) {
                    if (new_pswd.matches("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*[@#$%^&+=])(?=\\S+$).{8,25}$")) {
                        if (!conf_pswd.isEmpty()) {
                            if (new_pswd.equals(conf_pswd)) {
                                if (CommonUtilities.isConnectionAvailable(getActivity())) {
                                    invokeEmailRecovery(tkn, new_pswd, dialog);
                                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                                } else {
                                    CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.internetconnection));
                                }
                            } else {
                                CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.unmatch_conf_pswd));
                            }
                        } else {
                            CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.empty_conf_pswd));
                        }
                    } else {
                        CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.invalid_new_pswd));
                    }
                } else {
                    CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.empty_new_pswd));
                }

            } else {
                CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.unmatch_old_pswd));
            }
        } else {
            CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.empty_old_pswd));
        }

    }

    private void invokeEmailRecovery(String tkn, final String new_pswd, final DialogPlus dialog) {
        try {
            JSONObject params = new JSONObject();
            try {
                params.put("password", new_pswd);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressDialog = ProgressDialog.show(getActivity(), "", getResources().getString(R.string.please_wait), true);
            UserControllerApi apiService = XcomApiClient.getClient().create(UserControllerApi.class);
            Call<ResponseBody> apiResponse = apiService.updatePassword(params.toString(), CONSTANTS.DeviantMulti + tkn);
            apiResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        String responsevalue = response.body().string();

                        if (!responsevalue.isEmpty() && responsevalue != null) {
                            progressDialog.dismiss();

                            JSONObject jsonObject = new JSONObject(responsevalue);
                            loginResponseMsg = jsonObject.getString("msg");
                            loginResponseStatus = jsonObject.getString("status");
                            if (loginResponseStatus.equals("true")) {
                                editor.putString(CONSTANTS.pswd, new_pswd);
                                editor.apply();
                                CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.pswd_changed_succcess));
                                dialog.dismiss();
                            } else {
                                CommonUtilities.ShowToastMessage(getActivity(), loginResponseMsg);
                            }


                        } else {
                            CommonUtilities.ShowToastMessage(getActivity(), loginResponseMsg);
//                            Toast.makeText(getApplicationContext(), responsevalue, Toast.LENGTH_LONG).show();
                            Log.i(CONSTANTS.TAG, "onResponse:\n" + responsevalue);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.errortxt));
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.errortxt), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    if (t instanceof SocketTimeoutException) {
                        progressDialog.dismiss();
                        CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.Timeout));
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.Timeout), Toast.LENGTH_SHORT).show();
                    } else if (t instanceof java.net.ConnectException) {
                        progressDialog.dismiss();
                        CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.networkerror));
                        Toast.makeText(getActivity(), getResources().getString(R.string.networkerror), Toast.LENGTH_SHORT).show();
                    } else {
                        progressDialog.dismiss();
                        CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.errortxt));
//                        Toast.makeText(getActivity(), getResources().getString(R.string.errortxt), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception ex) {
            progressDialog.dismiss();
            ex.printStackTrace();
            CommonUtilities.ShowToastMessage(getActivity(), getResources().getString(R.string.errortxt));
//            Toast.makeText(getActivity(), getResources().getString(R.string.errortxt), Toast.LENGTH_SHORT).show();
        }


    }


}
