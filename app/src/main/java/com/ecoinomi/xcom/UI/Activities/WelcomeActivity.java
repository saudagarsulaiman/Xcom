package com.ecoinomi.xcom.UI.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ecoinomi.xcom.R;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WelcomeActivity extends AppCompatActivity {


    @BindView(R.id.btn_get_started)
    Button btn_get_started;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        ButterKnife.bind(this);

        btn_get_started.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Creating A Custom Dialog Using DialogPlus
                ViewHolder viewHolder = new ViewHolder(R.layout.dialog_get_started);
                final DialogPlus dialog = DialogPlus.newDialog(WelcomeActivity.this)
                        .setContentHolder(viewHolder)
                        .setGravity(Gravity.BOTTOM)
                        .setCancelable(true)
                        .setInAnimation(R.anim.slide_in_bottom)
                        .setOutAnimation(R.anim.slide_out_bottom)
                        .setContentWidth(ViewGroup.LayoutParams.MATCH_PARENT)
                        .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                        .create();

//                Initializing Widgets
                View view = dialog.getHolderView();
                Button signup_email_btn = view.findViewById(R.id.signup_email_btn);
//                LinearLayout facebook_lnr_lyt = view.findViewById(R.id.facebook_lnr_lyt);
//                LinearLayout google_lnr_lyt = view.findViewById(R.id.google_lnr_lyt);
                TextView login_txt = view.findViewById(R.id.login_txt);
                TextView txt_tc = view.findViewById(R.id.txt_tc);
                final LinearLayout lnr_tc = view.findViewById(R.id.lnr_tc);


                SpannableString spannableString = new SpannableString(getResources().getString(R.string.by_registering));

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        lnr_tc.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void updateDrawState(final TextPaint textPaint) {
                        textPaint.setColor(getResources().getColor(R.color.sky_blue1));
                        textPaint.setUnderlineText(false);
                    }
                };
                spannableString.setSpan(clickableSpan, spannableString.length() - 36, spannableString.length() - 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                txt_tc.setText(spannableString, TextView.BufferType.SPANNABLE);
                txt_tc.setHighlightColor(Color.TRANSPARENT);
                txt_tc.setMovementMethod(LinkMovementMethod.getInstance());

//                Signing Up With Email
                signup_email_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent signup = new Intent(WelcomeActivity.this, SignUpEmailActivity.class);
                        startActivity(signup);
                    }
                });

/*
//                Signing With Facebook
                facebook_lnr_lyt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Intent sign_fb = new Intent(WelcomeActivity.this, DashBoardActivity.class);
//                        startActivity(sign_fb);
                    }
                });

//                Signing With Google Plus
                google_lnr_lyt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Intent sign_google = new Intent(WelcomeActivity.this, DashBoardActivity.class);
//                        startActivity(sign_google);
                    }
                });
*/

//                Signing With Existing Account
                login_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent sign_in = new Intent(WelcomeActivity.this, LoginActivity.class);
                        startActivity(sign_in);
                    }
                });

//                Displaying DialogPlus
                dialog.show();
            }
        });

    }

}